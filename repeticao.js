//comentário de 1 linha
/*
Bloco de comentário
*/

console.log("Repetição While");


let numero = 0;
while(numero <= 10){
    console.log(`Valor: " ${(numero)}`);
    numero++;
}

/*while(numero <= 10){
    //console.log(`Valor: " ${(numero)}`);
    if(numero % 2 == 0){
        console.log(`Valor nr: " ${(numero)} é PAR`);
    }else{
        console.log(`Valor nr: " ${(numero)} é ÍMPAR`);
    }
    numero++;
}*/


console.log("Repetição DoWhile");
let numero1 = 11;
do{
    console.log(`Valor: " ${(numero1)}`);
    numero1++;
}while(numero1 <= 10);


console.log("Repetição FOR");

for(let numero2 = 0; numero2 <= 10; numero2++){
    console.log(`Valor: " ${(numero2)}`);
}

